package sanity.user;

import api.ApiClient;
import app.user.User;
import app.user.UserApiHelper;
import com.sun.jndi.dns.DnsClient;
import common.RandomDataGenerator;
import db.DbClient;
import org.testng.Assert;
import org.testng.annotations.Test;
import report.TestReport;

public class CreateUserWithApiTest {

    @Test(groups = "sanity")
    public void createUserTest() {

        TestReport.info("=====> Generating random User");
        User user = RandomDataGenerator.generateFakeUser();
        TestReport.info("New user info: " + user.toString());
        TestReport.info("=====> Sending request to create new User");
        ApiClient.sendUserPostRequest(user.newUserRequestAsString());
//        Check data in response
        TestReport.info("=====> Verifying data in response");
        Assert.assertTrue(ApiClient.verifyResponseCode(201));
        Assert.assertTrue(ApiClient.verifyFieldInResponse("username", user.getUsername()));
        Assert.assertTrue(UserApiHelper.findUserInResponse(user));

//        Check record in DB
//        DbClient.verifyUserFieldInDb("user_login", "user10");

//        Check user in GUI
//        LoginPage.loginToWordPress();
//        DashboardPage.navigateToUsrPage();
//        UserPage.isOnPage();
//        UserPage.findUserInUserTable("user10");

    }


}
