package sanity;

import api.ApiClient;
import app.user.User;
import app.user.UserApiHelper;
import app.user.UserDbHelper;
import com.github.javafaker.Faker;
import org.testng.annotations.Test;
import report.TestReport;

public class ExperimentsTests {

    @Test(groups = "experiments")
    public void experimentTests() {
        TestReport.info("Test case info");
        TestReport.error("Test case error");
        TestReport.warn("Test case warn");
        TestReport.debug("Test case debug");
    }

    @Test
    public void fakeUserTest() {
        Faker faker = new Faker();

        System.out.println(faker.name().firstName());
        System.out.println(faker.name().lastName());
        System.out.println(faker.name().fullName());
        System.out.println(faker.name().username());

        System.out.println(faker.address().city());
        System.out.println(faker.address().country());
        System.out.println(faker.address().fullAddress());

    }

    @Test
    public void findUserByUsername() {
        User user = new User();
        user.setUsername("user");
        UserApiHelper.findUserInResponse(user);

    }

    @Test
    public void createUserObjectFrom() {
        UserDbHelper.createUserObjectByUserName("user");

    }

}
