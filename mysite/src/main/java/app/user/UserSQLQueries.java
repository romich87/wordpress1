package app.user;

public class UserSQLQueries {
    public static final String GET_ALL_USERS = "SELECT\n" +
            "u.user_login AS 'username',\n" +
            "u.display_name AS 'name',\n" +
            "fn.first_name AS 'firstName',\n" +
            "ln.last_name AS 'lastName',\n" +
            "u.user_email AS 'email',\n" +
            "u.user_url AS 'url',\n" +
            "des.description,\n" +
            "loc.locale,\n" +
            "nn.nickname,\n" +
            "u.user_nicename AS 'slug',\n" +
            "r.user_role AS 'roles',\n" +
            "u.user_pass AS 'password'\n" +
            "FROM wordpress.wp_users u\n" +
            "JOIN\n" +
            "(\n" +
            "\tSELECT user_id, meta_value AS 'first_name'\n" +
            "\tFROM wp_usermeta\n" +
            "\tWHERE meta_key = 'first_name'\n" +
            ") AS fn\n" +
            "ON fn.user_id = u.ID\n" +
            "JOIN\n" +
            "(\n" +
            "\tSELECT user_id, meta_value AS 'last_name'\n" +
            "\tFROM wp_usermeta\n" +
            "\tWHERE meta_key = 'last_name'\n" +
            ") AS ln\n" +
            "ON ln.user_id = u.ID\n" +
            "JOIN\n" +
            "(\n" +
            "\tSELECT user_id, meta_value AS 'user_role'\n" +
            "\tFROM wp_usermeta\n" +
            "\tWHERE meta_key = 'wp_capabilities'\n" +
            ") AS r\n" +
            "ON r.user_id = u.ID\n" +
            "JOIN\n" +
            "(\n" +
            "\tSELECT user_id, meta_value AS 'nickname'\n" +
            "\tFROM wp_usermeta\n" +
            "\tWHERE meta_key = 'nickname'\n" +
            ") AS nn\n" +
            "ON nn.user_id = u.ID\n" +
            "JOIN\n" +
            "(\n" +
            "\tSELECT user_id, meta_value AS 'locale'\n" +
            "\tFROM wp_usermeta\n" +
            "\tWHERE meta_key = 'locale'\n" +
            ") AS loc\n" +
            "ON loc.user_id = u.ID\n" +
            "JOIN\n" +
            "(\n" +
            "\tSELECT user_id, meta_value AS 'description'\n" +
            "\tFROM wp_usermeta\n" +
            "\tWHERE meta_key = 'description'\n" +
            ") AS des\n" +
            "ON des.user_id = u.ID";
}
